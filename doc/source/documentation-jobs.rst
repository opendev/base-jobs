Documentation Jobs
==================

.. zuul:autojob:: opendev-nox-docs
.. zuul:autojob:: opendev-publish-nox-docs-base
.. zuul:autojob:: opendev-publish-nox-docs
.. zuul:autojob:: opendev-publish-unversioned-nox-docs
.. zuul:autojob:: opendev-tox-docs
.. zuul:autojob:: opendev-publish-tox-docs-base
.. zuul:autojob:: opendev-publish-tox-docs
.. zuul:autojob:: opendev-publish-unversioned-tox-docs
.. zuul:autojob:: opendev-promote-docs-base
.. zuul:autojob:: opendev-promote-docs
